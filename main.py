import sys
import youtube_dl
import json
from os import listdir
from os.path import isfile, join
from pydub import AudioSegment
tmp_songs = '/tmp/songs/'

def time_sort(e):
    return e.duration_seconds

def mix_files():
    audio_files = [AudioSegment.from_mp3(tmp_songs + f) for f in listdir(tmp_songs) if isfile(join(tmp_songs, f))]
    if(len(audio_files) < 2):
        return print('Need at least 2 songs to merge.');
    audio_files.sort(key=time_sort,reverse=True)
    base = audio_files.pop(0);
    for file in audio_files:
        base = base.overlay(file,position=0)
    base.export("output/mixed_sound.mp3", format="mp3")

def bad_input():
    print("Invalid arguments. Run with a youtube playlist url.")

def is_audio(x):
    return x == 'm4a' or x == 'mp3' or x == 'wav'

def main():
    print(str(sys.argv))
    if len(sys.argv) < 2:
        return bad_input()
    #ydl = youtube_dl.YoutubeDL({'outtmpl': '%(id)s.%(ext)s'})
    #videos = []
    #audios = []
    #with ydl:
    #    result = ydl.extract_info(
    #            sys.argv[1],
    #            download=False # We just want to extract the info
    #            )

    #    if 'entries' in result:
    #        # Can be a playlist or a list of videos
    #        videos += result['entries']
    #    else:
    #        # Just a video
    #        videos.append(result);
    #    for video in videos:
    #        #print(video)
    #        audios.append((video['id'],([x['format_id'] for x in video['formats'] if is_audio(x['ext'])])[-1]))
    #        #with open('output/data.json', 'w') as outfile:
    #        #    json.dump(video, outfile)
    #        #video_url = video['url']
    #        #print(video_url)
    #    print(audios);
    ydl_opts = {
            'format': 'bestaudio/best',
            'outtmpl':tmp_songs + '%(id)s.%(ext)s',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192',
                }],
            }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([sys.argv[1]])
    mix_files()


main()

