FROM python:rc-alpine

RUN apk update

RUN apk add ffmpeg

COPY . /app

WORKDIR /app

RUN pip install -r requirements.txt

RUN mkdir /tmp/songs

ENTRYPOINT ["python", "main.py"]
